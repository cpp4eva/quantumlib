﻿using System;

namespace AlexanderWood.Common.QuantumEncryption.Exceptions
{
    public class QuantumEncryptionException : Exception
    {
        public QuantumEncryptionException(string message) : base(message)
        {

        }
    }
}
