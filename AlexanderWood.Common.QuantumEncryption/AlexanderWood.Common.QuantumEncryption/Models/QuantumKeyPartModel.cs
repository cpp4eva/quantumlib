﻿using System;
using System.Collections.Generic;

namespace AlexanderWood.Common.QuantumEncryption.Models
{
    [Serializable]
    public class QuantumKeyPartModel
    {
        public long KeyAsLong { get; internal set; }
        public string KeyAsBinaryString { get; internal set; }
        public string KeyBasisThatShouldBeSent { get; internal set; }
        public List<PhotonDirection> PhotonDirections { get; internal set; }
    }
}
