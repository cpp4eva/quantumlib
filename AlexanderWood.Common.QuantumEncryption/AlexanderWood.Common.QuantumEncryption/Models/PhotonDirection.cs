﻿
namespace AlexanderWood.Common.QuantumEncryption.Models
{
    public enum PhotonDirection
    {
        Orthoganal = 0,
        Angled = 1
    }
}
