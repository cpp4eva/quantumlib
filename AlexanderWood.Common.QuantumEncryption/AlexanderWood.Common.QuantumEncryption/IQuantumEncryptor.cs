﻿
using AlexanderWood.Common.QuantumEncryption.Models;
using System.Threading.Tasks;

namespace AlexanderWood.Common.QuantumEncryption
{
    public interface IQuantumEncryptor
    {
        Task<QuantumKeyPartModel> GenerateInitialKeyPart(long keySize);
        string MergeKeysIntoAESKey(string keyPart1, string keyPart2);
    }
}
