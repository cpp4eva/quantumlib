﻿using AlexanderWood.Common.QuantumEncryption.Exceptions;
using AlexanderWood.Common.QuantumEncryption.Models;
using Microsoft.Quantum.Simulation.Simulators;
using Quantum.AlexanderWood.Common.QuantumEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlexanderWood.Common.QuantumEncryption.Implementations
{
    public class BellQuantumEncryptor : IQuantumEncryptor
    {
        private readonly long _amountWrongToIndicateFailure;
        public BellQuantumEncryptor(long amountWrongToIndicateFailure)
        {
            _amountWrongToIndicateFailure = amountWrongToIndicateFailure;
        }

        public async Task<QuantumKeyPartModel> GenerateInitialKeyPart(long keySize)
        {
            StringBuilder keyPart = new StringBuilder();
            string keyInBinary = "";
            long sumOfKeyAsLong = 0;
            List<PhotonDirection> list = new List<PhotonDirection>();
            //	//u1(v2w3−w2v3)−v1(u2v3−w2u3)+w1(u2v3−v2u3)≠0	   
            // We begin by defining a quantum simulator to be our target
            // machine.
            using (var simulator = new QuantumSimulator(throwOnReleasingQubitsNotInZeroState: true))
            {
                try
                {

                    (keyInBinary, sumOfKeyAsLong) = await QuantumEncryptorIntitialGeneration.Run(simulator, keySize);
                    for (var m = 0; m < keySize; m++)
                    {
                        var matrix = new long[3, 3];
                        for (var i = 0; i < 3; i++)
                        {
                            for (var j = 0; j < 3; j++)
                            {

                                matrix[i, j] = await GenerateRandomQuantumNumber.Run(simulator);

                            }
                        }
                        var coinFlip = await FlipQuantumCoin.Run(simulator);
                        bool detIsZero = coinFlip == 1 || GenerateDeterminant(matrix) == 0;
                        if (detIsZero)
                        {
                            keyPart.Append("1");
                            list.Add(PhotonDirection.Orthoganal);
                        }
                        else
                        {
                            keyPart.Append("0");
                            list.Add(PhotonDirection.Angled);
                        }
                    }
                }
                catch(Exception ex)
                {
                    //Logger
                }
            }

            return
                new QuantumKeyPartModel
                    {
                        KeyAsBinaryString = keyInBinary,
                        KeyAsLong = sumOfKeyAsLong,
                        KeyBasisThatShouldBeSent = keyPart.ToString(),
                        PhotonDirections = list
                    };
                
        }
        //u1(v2w3−w2v3)−v1(u2v3−w2u3)+w1(u2v3−v2u3)≠0	 
        private long GenerateDeterminant(long[,] matrix)
        {
            if(matrix.Length != 3)
            {
                throw new QuantumEncryptionException("Must Be A 3D Vector");
            }
            var a = matrix[0, 0] * ((matrix[1, 1] * matrix[2, 2]) - (matrix[1,2]*matrix[2,1]));
            var b = matrix[0, 1] * ((matrix[1, 0] * matrix[2, 2]) - (matrix[2,1]*matrix[2,0]));
            var c = matrix[0, 2] * ((matrix[1, 0] * matrix[1, 2]) - (matrix[1, 1] * matrix[2, 0]));
            return
                a - b + c;
        }

        public string MergeKeysIntoAESKey(string keyPart1, string keyPart2)
        {
            if(keyPart1.Length != keyPart2.Length)
            {
                throw new QuantumEncryptionException("The Keys are Not The Same Size");
            }
            StringBuilder sb = new StringBuilder();

            for(var i = 0; i < keyPart2.Length; i++)
            {
                if(keyPart2[i] == keyPart1[i])
                {
                    sb.Append(keyPart1[i]);
                }
            }
            if(sb.Length < _amountWrongToIndicateFailure)
            {
                throw new QuantumEncryptionException("The Length Generated For The Remainder Key Is Too Small");
            }
            return
                sb.ToString();
        }
    }
}
