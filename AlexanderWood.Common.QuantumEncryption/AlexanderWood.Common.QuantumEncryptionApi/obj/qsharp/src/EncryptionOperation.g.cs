#pragma warning disable 1591
using System;
using Microsoft.Quantum.Core;
using Microsoft.Quantum.Primitive;
using Microsoft.Quantum.Simulation.Core;

[assembly: Microsoft.Quantum.QsCompiler.Attributes.CallableDeclaration("{\"Kind\":{\"Case\":\"Operation\"},\"QualifiedName\":{\"Namespace\":\"Quantum.AlexanderWood.Common.QuantumEncryption\",\"Name\":\"QuantumEncryptorIntitialGeneration\"},\"SourceFile\":\"C:/Projects/AlexanderWood.Common.QuantumEncryption/AlexanderWood.Common.QuantumEncryptionApi/EncryptionOperation.qs\",\"Position\":{\"Item1\":5,\"Item2\":4},\"SymbolRange\":{\"Item1\":{\"Line\":1,\"Column\":11},\"Item2\":{\"Line\":1,\"Column\":45}},\"ArgumentTuple\":{\"Case\":\"QsTuple\",\"Fields\":[[{\"Case\":\"QsTupleItem\",\"Fields\":[{\"VariableName\":{\"Case\":\"ValidName\",\"Fields\":[\"n\"]},\"Type\":{\"Case\":\"Int\"},\"IsMutable\":false,\"HasLocalQuantumDependency\":false,\"Position\":{\"Case\":\"Null\"},\"Range\":{\"Item1\":{\"Line\":1,\"Column\":47},\"Item2\":{\"Line\":1,\"Column\":48}}}]}]]},\"Signature\":{\"TypeParameters\":[],\"ArgumentType\":{\"Case\":\"Int\"},\"ReturnType\":{\"Case\":\"TupleType\",\"Fields\":[[{\"Case\":\"String\"},{\"Case\":\"Int\"}]]},\"SupportedFunctors\":[]},\"Documentation\":[]}")]
[assembly: Microsoft.Quantum.QsCompiler.Attributes.SpecializationDeclaration("{\"Kind\":{\"Case\":\"QsBody\"},\"Parent\":{\"Namespace\":\"Quantum.AlexanderWood.Common.QuantumEncryption\",\"Name\":\"QuantumEncryptorIntitialGeneration\"},\"SourceFile\":\"C:/Projects/AlexanderWood.Common.QuantumEncryption/AlexanderWood.Common.QuantumEncryptionApi/EncryptionOperation.qs\",\"Position\":{\"Item1\":5,\"Item2\":4},\"HeaderRange\":{\"Item1\":{\"Line\":1,\"Column\":11},\"Item2\":{\"Line\":1,\"Column\":45}},\"Documentation\":[]}")]
[assembly: Microsoft.Quantum.QsCompiler.Attributes.CallableDeclaration("{\"Kind\":{\"Case\":\"Operation\"},\"QualifiedName\":{\"Namespace\":\"Quantum.AlexanderWood.Common.QuantumEncryption\",\"Name\":\"FlipQuantumCoin\"},\"SourceFile\":\"C:/Projects/AlexanderWood.Common.QuantumEncryption/AlexanderWood.Common.QuantumEncryptionApi/EncryptionOperation.qs\",\"Position\":{\"Item1\":43,\"Item2\":1},\"SymbolRange\":{\"Item1\":{\"Line\":1,\"Column\":11},\"Item2\":{\"Line\":1,\"Column\":26}},\"ArgumentTuple\":{\"Case\":\"QsTuple\",\"Fields\":[[]]},\"Signature\":{\"TypeParameters\":[],\"ArgumentType\":{\"Case\":\"UnitType\"},\"ReturnType\":{\"Case\":\"Int\"},\"SupportedFunctors\":[]},\"Documentation\":[]}")]
[assembly: Microsoft.Quantum.QsCompiler.Attributes.SpecializationDeclaration("{\"Kind\":{\"Case\":\"QsBody\"},\"Parent\":{\"Namespace\":\"Quantum.AlexanderWood.Common.QuantumEncryption\",\"Name\":\"FlipQuantumCoin\"},\"SourceFile\":\"C:/Projects/AlexanderWood.Common.QuantumEncryption/AlexanderWood.Common.QuantumEncryptionApi/EncryptionOperation.qs\",\"Position\":{\"Item1\":43,\"Item2\":1},\"HeaderRange\":{\"Item1\":{\"Line\":1,\"Column\":11},\"Item2\":{\"Line\":1,\"Column\":26}},\"Documentation\":[]}")]
[assembly: Microsoft.Quantum.QsCompiler.Attributes.CallableDeclaration("{\"Kind\":{\"Case\":\"Operation\"},\"QualifiedName\":{\"Namespace\":\"Quantum.AlexanderWood.Common.QuantumEncryption\",\"Name\":\"GenerateRandomQuantumNumber\"},\"SourceFile\":\"C:/Projects/AlexanderWood.Common.QuantumEncryption/AlexanderWood.Common.QuantumEncryptionApi/EncryptionOperation.qs\",\"Position\":{\"Item1\":63,\"Item2\":1},\"SymbolRange\":{\"Item1\":{\"Line\":1,\"Column\":11},\"Item2\":{\"Line\":1,\"Column\":38}},\"ArgumentTuple\":{\"Case\":\"QsTuple\",\"Fields\":[[]]},\"Signature\":{\"TypeParameters\":[],\"ArgumentType\":{\"Case\":\"UnitType\"},\"ReturnType\":{\"Case\":\"Int\"},\"SupportedFunctors\":[]},\"Documentation\":[]}")]
[assembly: Microsoft.Quantum.QsCompiler.Attributes.SpecializationDeclaration("{\"Kind\":{\"Case\":\"QsBody\"},\"Parent\":{\"Namespace\":\"Quantum.AlexanderWood.Common.QuantumEncryption\",\"Name\":\"GenerateRandomQuantumNumber\"},\"SourceFile\":\"C:/Projects/AlexanderWood.Common.QuantumEncryption/AlexanderWood.Common.QuantumEncryptionApi/EncryptionOperation.qs\",\"Position\":{\"Item1\":63,\"Item2\":1},\"HeaderRange\":{\"Item1\":{\"Line\":1,\"Column\":11},\"Item2\":{\"Line\":1,\"Column\":38}},\"Documentation\":[]}")]
#line hidden
namespace Quantum.AlexanderWood.Common.QuantumEncryption
{
    public class QuantumEncryptorIntitialGeneration : Operation<Int64, (String,Int64)>, ICallable
    {
        public QuantumEncryptorIntitialGeneration(IOperationFactory m) : base(m)
        {
        }

        public class Out : QTuple<(String,Int64)>, IApplyData
        {
            public Out((String,Int64) data) : base(data)
            {
            }

            System.Collections.Generic.IEnumerable<Qubit> IApplyData.Qubits => null;
        }

        String ICallable.Name => "QuantumEncryptorIntitialGeneration";
        String ICallable.FullName => "Quantum.AlexanderWood.Common.QuantumEncryption.QuantumEncryptorIntitialGeneration";
        protected Allocate Allocate
        {
            get;
            set;
        }

        protected IUnitary<Qubit> MicrosoftQuantumPrimitiveH
        {
            get;
            set;
        }

        protected ICallable<Qubit, Result> M
        {
            get;
            set;
        }

        protected Release Release
        {
            get;
            set;
        }

        public override Func<Int64, (String,Int64)> Body => (__in) =>
        {
            var n = __in;
#line 8 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            var result = "";
#line 9 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            var sum = 0L;
#line 10 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            var count = 0L;
#line hidden
            {
#line 12 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                var qubit = Allocate.Apply(1L);
#line 14 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                foreach (var i in new Range(0L, (n - 1L)))
#line hidden
                {
#line 16 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    if ((count == 0L))
                    {
#line 18 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                        count = 1L;
                    }
                    else
                    {
#line 22 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                        count = (count * 2L);
                    }

#line 25 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    MicrosoftQuantumPrimitiveH.Apply(qubit[0L]);
#line 26 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    var res = M.Apply(qubit[0L]);
#line 27 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    if ((res == Result.One))
                    {
#line 29 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                        result = (result + "1");
#line 30 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                        sum = (sum + count);
                    }
                    else
                    {
#line 34 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                        result = (result + "0");
                    }
                }

#line hidden
                Release.Apply(qubit);
            }

#line 40 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            return (result, sum);
        }

        ;
        public override void Init()
        {
            this.Allocate = this.Factory.Get<Allocate>(typeof(Microsoft.Quantum.Primitive.Allocate));
            this.MicrosoftQuantumPrimitiveH = this.Factory.Get<IUnitary<Qubit>>(typeof(Microsoft.Quantum.Primitive.H));
            this.M = this.Factory.Get<ICallable<Qubit, Result>>(typeof(Microsoft.Quantum.Primitive.M));
            this.Release = this.Factory.Get<Release>(typeof(Microsoft.Quantum.Primitive.Release));
        }

        public override IApplyData __dataIn(Int64 data) => new QTuple<Int64>(data);
        public override IApplyData __dataOut((String,Int64) data) => new Out(data);
        public static System.Threading.Tasks.Task<(String,Int64)> Run(IOperationFactory __m__, Int64 n)
        {
            return __m__.Run<QuantumEncryptorIntitialGeneration, Int64, (String,Int64)>(n);
        }
    }

    public class FlipQuantumCoin : Operation<QVoid, Int64>, ICallable
    {
        public FlipQuantumCoin(IOperationFactory m) : base(m)
        {
        }

        String ICallable.Name => "FlipQuantumCoin";
        String ICallable.FullName => "Quantum.AlexanderWood.Common.QuantumEncryption.FlipQuantumCoin";
        protected Allocate Allocate
        {
            get;
            set;
        }

        protected IUnitary<Qubit> MicrosoftQuantumPrimitiveH
        {
            get;
            set;
        }

        protected ICallable<Qubit, Result> M
        {
            get;
            set;
        }

        protected Release Release
        {
            get;
            set;
        }

        public override Func<QVoid, Int64> Body => (__in) =>
        {
#line 46 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            var retVal = 0L;
#line hidden
            {
#line 47 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                var qubit = Allocate.Apply(1L);
#line 49 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                MicrosoftQuantumPrimitiveH.Apply(qubit[0L]);
#line 50 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                var k = M.Apply(qubit[0L]);
#line 51 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                if ((k == Result.Zero))
                {
#line 53 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    retVal = -(1L);
                }
                else
                {
#line 57 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    retVal = 1L;
                }

#line hidden
                Release.Apply(qubit);
            }

#line 60 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            return retVal;
        }

        ;
        public override void Init()
        {
            this.Allocate = this.Factory.Get<Allocate>(typeof(Microsoft.Quantum.Primitive.Allocate));
            this.MicrosoftQuantumPrimitiveH = this.Factory.Get<IUnitary<Qubit>>(typeof(Microsoft.Quantum.Primitive.H));
            this.M = this.Factory.Get<ICallable<Qubit, Result>>(typeof(Microsoft.Quantum.Primitive.M));
            this.Release = this.Factory.Get<Release>(typeof(Microsoft.Quantum.Primitive.Release));
        }

        public override IApplyData __dataIn(QVoid data) => data;
        public override IApplyData __dataOut(Int64 data) => new QTuple<Int64>(data);
        public static System.Threading.Tasks.Task<Int64> Run(IOperationFactory __m__)
        {
            return __m__.Run<FlipQuantumCoin, QVoid, Int64>(QVoid.Instance);
        }
    }

    public class GenerateRandomQuantumNumber : Operation<QVoid, Int64>, ICallable
    {
        public GenerateRandomQuantumNumber(IOperationFactory m) : base(m)
        {
        }

        String ICallable.Name => "GenerateRandomQuantumNumber";
        String ICallable.FullName => "Quantum.AlexanderWood.Common.QuantumEncryption.GenerateRandomQuantumNumber";
        protected Allocate Allocate
        {
            get;
            set;
        }

        protected IUnitary<Qubit> MicrosoftQuantumPrimitiveH
        {
            get;
            set;
        }

        protected ICallable<Qubit, Result> M
        {
            get;
            set;
        }

        protected Release Release
        {
            get;
            set;
        }

        protected ICallable<QVoid, Int64> FlipQuantumCoin
        {
            get;
            set;
        }

        public override Func<QVoid, Int64> Body => (__in) =>
        {
#line 66 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            var sum = 1L;
#line hidden
            {
#line 67 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                var qubit = Allocate.Apply(1L);
#line 69 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                var count = 1L;
#line 70 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                foreach (var i in new Range(2L, 15L))
#line hidden
                {
#line 72 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    MicrosoftQuantumPrimitiveH.Apply(qubit[0L]);
#line 73 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    var k = M.Apply(qubit[0L]);
#line 74 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    count = (count * 2L);
#line 75 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                    if ((k == Result.One))
                    {
#line 77 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
                        sum = (sum + (FlipQuantumCoin.Apply(QVoid.Instance) * count));
                    }
                }

#line hidden
                Release.Apply(qubit);
            }

#line 81 "C:\\Projects\\AlexanderWood.Common.QuantumEncryption\\AlexanderWood.Common.QuantumEncryptionApi\\EncryptionOperation.qs"
            return sum;
        }

        ;
        public override void Init()
        {
            this.Allocate = this.Factory.Get<Allocate>(typeof(Microsoft.Quantum.Primitive.Allocate));
            this.MicrosoftQuantumPrimitiveH = this.Factory.Get<IUnitary<Qubit>>(typeof(Microsoft.Quantum.Primitive.H));
            this.M = this.Factory.Get<ICallable<Qubit, Result>>(typeof(Microsoft.Quantum.Primitive.M));
            this.Release = this.Factory.Get<Release>(typeof(Microsoft.Quantum.Primitive.Release));
            this.FlipQuantumCoin = this.Factory.Get<ICallable<QVoid, Int64>>(typeof(FlipQuantumCoin));
        }

        public override IApplyData __dataIn(QVoid data) => data;
        public override IApplyData __dataOut(Int64 data) => new QTuple<Int64>(data);
        public static System.Threading.Tasks.Task<Int64> Run(IOperationFactory __m__)
        {
            return __m__.Run<GenerateRandomQuantumNumber, QVoid, Int64>(QVoid.Instance);
        }
    }
}