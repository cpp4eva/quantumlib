﻿namespace Quantum.AlexanderWood.Common.QuantumEncryption
{
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;

    operation QuantumEncryptorIntitialGeneration (n : Int) : (String, Int)
	{
		mutable result = "";
		mutable sum = 0;
		mutable count = 0;
		//mutable arr = [n]; 
		using(qubit = Qubit[1])
		{
			for(i in 0 .. n-1)
			{
				if(count == 0)
				{
					set count = 1;
				}
				else
				{
					set count = count * 2;
				}

				H(qubit[0]);
			 	let res = M(qubit[0]);
				if(res == One)
				{
				    set result = result + "1";
					set sum = sum + count;					
				}
				else
				{
					set result = result + "0";
				}
			}
			
		}

		return 
			(result, sum);
    }

	operation FlipQuantumCoin() : Int
	{
		mutable retVal = 0;
		using(qubit = Qubit[1])
		{
			H(qubit[0]);
			let k = M(qubit[0]);
			if(k == Zero)
			{
			 	set retVal = -1;
			}
			else
			{
				set retVal = 1;
			}
		}
		return 
			 retVal;
	}

	operation GenerateRandomQuantumNumber() : Int
	{
		mutable sum = 1;
		using(qubit = Qubit[1])
		{
			mutable count = 1;
			for(i in 2 .. 15)
			{
				H(qubit[0]);
				let k = M(qubit[0]);
				set count = count * 2;
				if(k == One)
				{
					set sum = sum + (FlipQuantumCoin() * count); 	
				}
			}
		}
		return 
			sum;
	}
}
